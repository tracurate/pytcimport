# Requirements

tcimport requires the following software on your system:

* python (3.6+)

# Installation

## Download the repository

To install tcimport, either clone the repository and change to it

```bash
git clone "https://gitlab.com/tracurate/pytcimport.git"
cd pytcimport
```

or download a zip of the repository, unpack, and change to it:

```bash
wget "https://gitlab.com/tracurate/pytcimport/-/archive/master/pytcimport-master.zip"
unzip pytcimport-master.zip
cd pytcimport-master
```

## Install via pip

From the source directory, you can install tcimport via pip:

```bash
pip3 install .
```

This should take care of also installing tcimport's dependencies.

## Usage without installation

You can also use tcimport without installation by adding the path to tcimport's project directory to your module search path:

```python
import sys
sys.path.append("/path/to/pytcimport")

import tcimport
```
