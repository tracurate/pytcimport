import warnings
import h5py
import numpy as np

####################################################################################
# public functions
####################################################################################

#' @title Events
#' @name read_events
#' @description
#' \code{read_events} reads the events from the given hdf5 file .
#' @param filename the path to the TraCurate project file
#' @return Returns a list of events, with event_id, name and description.
#' @examples
#' example=example_filename()
#' #read the events from a given project file
#' events=read_events(example)
#' @export
def read_events(filename):
    with h5py.File(name=filename, mode="r") as hdf5file:
        features = ["event_id", "name", "description"]
        event_paths = [f"/events/{e}" for e in hdf5file["/events"]]
        events = [dict(zip(
            features,
            [hdf5file[f"{path}/{feature}"][()] for feature in features]
        )) for path in event_paths]
        ret = dict(zip(
            [x["event_id"] for x in events],
            events
        ))

    return(ret)

#' @title Annotations
#' @name read_annotations
#' @description
#' \code{read_annotations} reads the annotations from the given hdf5 file .
#' @param filename the path to the TraCurate project file
#' @return Returns a list of object and track annotations, with annotation_id, name and description.
#' @examples
#' example=example_filename()
#' #read the annotations from a given project file
#' annotations=read_annotations(example)
#' @export
def read_annotations(filename):
    with h5py.File(name=filename, mode="r") as hdf5file:
        features = ["object_annotation_id", "title", "description"]
        obj_annotation_paths = [f"/annotations/object_annotations/{a}"
                                for a in hdf5file["/annotations/object_annotations"]]

        obj_annotations = [dict(zip(
            features,
            [hdf5file[f"{path}/{feature}"][()] for feature in features]
        )) for path in obj_annotation_paths]

        features = ["track_annotation_id", "title", "description"]
        track_annotation_paths = [f"/annotations/track_annotations/{a}"
                                  for a in hdf5file["/annotations/track_annotations"]]

        track_annotations = [dict(zip(
            features,
            [hdf5file[f"{path}/{feature}"][()] for feature in features]
        )) for path in track_annotation_paths]

        annotations = {
            "object_annotations": dict(zip(
                [x["object_annotation_id"] for x in obj_annotations],
                obj_annotations
            )),
            "track_annotations": dict(zip(
                [x["track_annotation_id"] for x in track_annotations],
                track_annotations
            ))}

    return(annotations)

#' @title Image dimension
#' @name read_dimensions
#' @description
#' \code{read_dimension} reads the dimension of an image from the given hdf5 file. The data format is constructed such that each
#' slice in a frame can have arbitrary dimensions. But in principle image dimensions are likely to be the same over all frames and slices.
#' @param filename the path to the TraCurate project file
#' @param frame_id the frame id the object resides in (0..n).
#' @param slice_id the slice id the object resides in (0..n).
#' @return the image dimensions
#' @examples
#' example=example_filename()
#' read_dimensions(example, 0, 0)
#' @export
def read_dimensions(filename, frame_id, slice_id):
    with h5py.File(name=filename, mode="r") as hdf5file:
        dimension_path = f"/objects/frames/{frame_id}/slices/{slice_id}/dimensions"
        dimension = hdf5file[dimension_path][()]

    return dimension


#' @title Image
#' @name read_image_by_id
#' @description
#' \code{read_image_by_id} reads an image from the given hdf5 file .
#' @param filename the path to the TraCurate project file
#' @param frame_id the frame id the object resides in (0..n).
#' @param slice_id the slice id the object resides in (0..n).
#' @param channel_id the channel id the object resides in (0..n).
#' @return Returns an image.
#' @examples
#' example=example_filename()
#' #reading and ploting an image from a frame with only one slice and one channel
#' #read read_image_by_id here implicitly loads the image from frameID=0, sliceID=0, channelID=0
#' read_image_by_id(example, 1)
#'
#' #read image and plot histogram
#' img=read_image_by_id(example, 0, as_matrix=TRUE)
#' hist(img)
#' @export
def read_image_by_id(filename, frame_id, slice_id=0, channel_id=0, as_matrix=False):
    with h5py.File(name=filename, mode="r") as hdf5file:
        nslices = hdf5file["/objects/nslices"][()]
        if slice_id < nslices:
            nchannels = hdf5file[f"/objects/frames/{frame_id}/slices/{slice_id}/nchannels"][()]
            if channel_id < nchannels:
                img = hdf5file[f"/images/frames/{frame_id}/slices/{slice_id}/channels/{channel_id}"][()]
                if as_matrix:
                    return(img)
                else:
                    import matplotlib.pyplot as plt
                    plt.imshow(img)
            else:
                raise ValueError(f"Dataset has {nchannels} channels, please specify a channel_id in the range of 0 to {nchannels - 1}!")
        else:
            raise ValueError(f"Dataset has {nslices} slices, please specify a slice_id in the range of 0 to {nslices - 1}!")

#' @title Image
#' @name read_image_by_path
#' @description
#' \code{read_image_by_path} reads an image from the given hdf5 file .
#' @param filename the path to the TraCurate project file
#' @param image_path path to the image
#' @return Returns an image.
#' @examples
#' example=example_filename()
#' #reading and ploting an image from a frame with only one slice and one channel
#' #read read_image_by_id here implicitly loads the image from frameID=0, sliceID=0, channelID=0
#' read_image_by_path(example, "/images/frames/0/slices/0/channels/0")
#'
#' #read image and plot histogram
#' img=read_image_by_path(example, "/images/frames/0/slices/0/channels/0", as_matrix=TRUE)
#' hist(img)
#' @export
def read_image_by_path(filename, image_path, as_matrix=False):
    with h5py.File(name=filename, mode="r") as hdf5file:
        img = hdf5file[image_path][()]
        if as_matrix:
            return(img)
        else:
            import matplotlib.pyplot as plt
            plt.imshow(img)

#' @title Object
#' @name read_object_by_id
#' @description
#' \code{read_object_by_id} reads the objects features for the specified object from the given hdf5 file .
#' @param filename the path to the TraCurate project file
#' @param object_id the object id of interest (0..n).
#' @param frame_id the frame id the object resides in (0..n).
#' @param slice_id the slice id the object resides in (0..n).
#' @param channel_id the channel id the object resides in (0..n).
#' @param features the feature to extract from the object. Available features are \code{"annotations", "bounding_box", "centroid", "channel_id", "frame_id", "object_id", "outline", "slice_id"}, \code{"all"} if all features shall be extracted or \code{"none"} if no features shall be extracted.
#' @param image_dimension the original image dimensions of the image the object resides in. It's save to use \code{NULL} here, then the image dimensions are automatically determined.
#' @return Returns a list of the specified features for the given object.
#' @examples
#' example=example_filename()
#' #read all features for an specific object from a given frame/slice/channel
#' object=read_object_by_path(example, "/objects/frames/0/slices/0/channels/0/objects/1")
#' object=read_object_by_id(example, 1, 0)
#'
#' #plot object outline onto image
#' img = read_image_by_id(example, 0, as_matrix = TRUE) #read the image
#' image(img, col  = gray((0:32)/32))
#' lines(object$outline[1,]/dim(img)[1], object$outline[2,]/dim(img)[2], type = "l")
#' @export
def read_object_by_id(filename, object_id, frame_id, slice_id=0, channel_id=0, features=["all"], image_dimension=None):
    with h5py.File(name=filename, mode="r") as hdf5file:
        nslices = hdf5file["/objects/nslices"][()]
        if slice_id < nslices:
            nchannels = hdf5file[f"/objects/frames/{frame_id}/slices/{slice_id}/nchannels"][()]
            if channel_id < nchannels:
                object = _read_object_by_path(hdf5file,
                                              f"/objects/frames/{frame_id}/slices/{slice_id}/channels/{channel_id}/objects/{object_id}",
                                              features)
                return(object)
            else:
                raise ValueError(f"Dataset has {nchannels} channels, please specify a channel_id in the range of 0 to {nchannels - 1}!")
        else:
            raise ValueError(f"Dataset has {nslices} slices, please specify a slice_id in the range of 0 to {nslices - 1}!")


#' @title Object
#' @name read_object_by_path
#' @description
#' \code{read_object_by_path} reads the objects features for the specified object from the given hdf5 file .
#' @param filename the path to the TraCurate project file
#' @param object path in the hdf5 file
#' @param features the feature to extract from the object. Available features are \code{"annotations", "bounding_box", "centroid", "channel_id", "frame_id", "object_id", "outline", "slice_id"}, \code{"all"} if all features shall be extracted or \code{"none"} if no features shall be extracted.
#' @param image_dimension the original image dimensions of the image the object resides in. It's save to use \code{NULL} here, then the image dimensions are automatically determined.
#' @return Returns a list of the specified features for the given object.
#' @examples
#' example=example_filename()
#' #read all features for an specific object from a given frame/slice/channel
#' object=read_object_by_path(example, "/objects/frames/0/slices/0/channels/0/objects/1")
#'
#' #plot object outline onto image
#' img = read_image_by_id(example, 0, as_matrix = TRUE) #read the image
#' image(img, col  = gray((0:32)/32))
#' lines(object$outline[1,]/dim(img)[1], object$outline[2,]/dim(img)[2], type = "l")
#' @export
def read_object_by_path(filename, object_path, features=["all"], image_dimension=None):
    with h5py.File(name=filename, mode="r") as hdf5file:
        object = _read_object_by_path(hdf5file, object_path, features=features, image_dimension=None)
    return(object)

#' @title Objects
#' @name read_objects
#' @description
#' \code{read_objects} reads the objects features for the specified channel or track path from the given hdf5 file . All the objects for the specific channel or track will be returned.
#' @param filename the path to the TraCurate project file
#' @param path in the hdf5 file
#' @param features the feature to extract from the object. Available features are \code{"annotations", "bounding_box", "centroid", "channel_id", "frame_id", "object_id", "outline", "slice_id"}, \code{"all"} if all features shall be extracted or \code{"none"} if no features shall be extracted.
#' @param image_dimension the original image dimensions of the image the object resides in. It's save to use \code{NULL} here, then the image dimensions are automatically determined.
#' @return Returns a list of objects with the specified features.
#' @examples
#' example=example_filename()
#' #read all object outlines and masks from a given frame
#' objects=read_objects(example, "/objects/frames/0/slices/0/channels/0", feature="outline")
#'
#' #plot the outlines of all objects onto the image
#' img = read_image_by_id(example, 0, as_matrix = TRUE) #read the image
#' image(img, col  = gray((0:32)/32))
#' lapply(objects, function(o) lines(o$outline[1,]/dim(img)[1], o$outline[2,]/dim(img)[2], type = "l"))
#' @export
def read_objects(filename, path, features=["all"], image_dimension=None):
    with h5py.File(name=filename, mode="r") as hdf5file:
        objects = _read_objects(hdf5file, path, features, image_dimension)
    return(objects)

#' @title Objects
#' @name read_objects_by_id
#' @description
#' \code{read_objects_by_id} reads the objects features for the specified frame/slice/channel. All the objects for the specific channel will be returned.
#' @param filename the path to the TraCurate project file
#' @param frame_id the frame id the object resides in (0..n).
#' @param slice_id the slice id the object resides in (0..n).
#' @param channel_id the channel id the object resides in (0..n).
#' @param features the feature to extract from the object. Available features are \code{"annotations", "bounding_box", "centroid", "channel_id", "frame_id", "object_id", "outline", "slice_id"}, \code{"all"} if all features shall be extracted or \code{"none"} if no features shall be extracted.
#' @param image_dimension the original image dimensions of the image the object resides in. It's save to use \code{NULL} here, then the image dimensions are automatically determined.
#' @return Returns a list of objects with the specified features.
#' @examples
#' example=example_filename()
#' #read all object outlines and masks from a given frame
#' objects=read_objects_by_id(example, 0, feature="all")
#'
#' #plot the outlines of all objects onto the image
#' img = read_image_by_id(example, 0, as_matrix = TRUE) #read the image
#' image(img, col  = gray((0:32)/32))
#' lapply(objects, function(o) lines(o$outline[1,]/dim(img)[1], o$outline[2,]/dim(img)[2], type = "l"))
#' @export
def read_objects_by_id(filename, frame_id, slice_id=0, channel_id=0, features=["all"], image_dimension=None):
    with h5py.File(name=filename, mode="r") as hdf5file:
        nslices = hdf5file["/objects/nslices"][()]
        if slice_id < nslices:
            nchannels = hdf5file[f"/objects/frames/{frame_id}/slices/{slice_id}/nchannels"][()]
            if channel_id < nchannels:
                objects = _read_objects(hdf5file, f"/objects/frames/{frame_id}/slices/{slice_id}/channels/{channel_id}", features)
                return(objects)
            else:
                raise ValueError(f"Dataset has {nchannels} channels, please specify a channel_id in the range of 0 to {nchannels - 1}!")
        else:
            raise ValueError(f"Dataset has {nslices} slices, please specify a slice_id in the range of 0 to {nslices - 1}!")

#' @title Track
#' @name read_track
#' @description
#' \code{read_track} reads the objects features for the specified channel or track path from the given hdf5 file . All the objects for the specific channel or track will be returned.
#' @param filename the path to the TraCurate project file
#' @param path in the hdf5 file
#' @param track_features the feature to extract from the track Available features are \code{"annotations", "end", "id", "length", "next", "next_event", "previous", "previous_event", "start"}, \code{"all"} if all features shall be extracted or \code{"none"} if no features shall be extracted.
#' @param obj_features the feature to extract from the object. Available features are \code{"annotations", "bounding_box", "centroid", "channel_id", "frame_id", "object_id", "outline", "slice_id"}, \code{"all"} if all features shall be extracted or \code{"none"} if no features shall be extracted.
#' @param type the type of the tracks which are read, this is either \code{"autotracklets"} or \code{"tracklets"}. The "autotracklets" are the tracks originating from the specific automatic tracking algorithm and "tracklets" are the tracks which where manually curated.
#' @return Returns a list of objects with the specified features.
#' @examples
#' example=example_filename()
#' #read a specific track with all the track and no object features
#' track=read_track(example, "/tracklets/0", obj_features="none")
#' track$length #print track length
#' track$start #print starting frame id of the track
#'
#' #read a specific track with all object centroids
#' track=read_track(example, "/tracklets/0", track_features="none", obj_features="centroid")
#' centroids=matrix(unlist(track$centroid),ncol=2, byrow=TRUE)
#' dims=read_dimensions(example, 0, 0)
#' plot(centroids[,1],centroids[,2], type="l", xlim = c(0,dims[1]), ylim=c(0,dims[2]))
#' @export
def read_track(filename, track_path, track_features=["all"], obj_features=["all"]):
    with h5py.File(name=filename, mode="r") as hdf5file:
        track = _read_track(hdf5file, track_path, track_features, obj_features)
    return(track)

#' @title Tracks
#' @name read_tracks
#' @description
#' \code{read_tracks} reads the objects features for the specified channel or track path from the given hdf5 file . All the objects for the specific channel or track will be returned.
#' @param filename the path to the TraCurate project file
#' @param ids the id's of the tracks to be read. If \code{ids=-1} then all tracks will be read.
#' @param track_features the feature to extract from the track Available features are \code{"annotations, end, id, length, next, next_event, previous, previous_event, start"}, \code{"all"} if all features shall be extracted or \code{"none"} if no features shall be extracted.
#' @param obj_features the feature to extract from the object. Available features are \code{"annotations, bounding_box, centroid, channel_id, frame_id, mask, object_id, outline, slice_id"}, \code{"all"} if all features shall be extracted or \code{"none"} if no features shall be extracted.
#' @param type the type of the tracks which are read, this is either \code{"autotracklets"} or \code{"tracklets"}. The "autotracklets" are the tracks originating from the specific automatic tracking algorithm and "tracklets" are the tracks which where manually curated.
#' @return Returns a list of objects with the specified features.
#' @examples
#' example=example_filename()
#' #read tracks with all the track and no object features
#' autotracks=read_tracks(example, obj_features="none", type="autotracklets") #read automatically generated tracks
#' tracks=read_tracks(example, obj_features="none", type="tracklets") #read curated tracks
#' length_tracks=unlist(lapply(tracks, function(track) track$length)) #get track lengths
#' length_autotracks=unlist(lapply(autotracks, function(track) track$length)) #get autotrack lengths
#' median(length_tracks)-median(length_autotracks) #compare median track lengths
#'
#' #plot tracks and autotracks length distribution
#' hist(length_tracks)
#' hist(length_autotracks)
#'
#' #read all tracks with all object centroids
#' tracks=read_tracks(example, track_features="none", obj_features="centroid")
#' track_centroids=lapply(tracks, function(track) matrix(unlist(track$centroid),ncol=2, byrow=TRUE))
#' dims=read_dimensions(example, 0, 0)
#'
#' #plot object displacement for all cells
#' plot(track_centroids[[1]][,1],track_centroids[[1]][,2], type="l", xlim = c(0,dims[1]), ylim=c(0,dims[2]), xlab="x", ylab="y")
#' dummy=lapply(track_centroids[2:length(track_centroids)], function(c) lines(c[,1],c[,2], xlim = c(0,dims[1]), ylim=c(0,dims[2])))
#' @export
def read_tracks(filename, ids=[-1], track_features=["all"], obj_features=["all"], type="tracklets"):
    with h5py.File(name=filename, mode="r") as hdf5file:
        tracks = _read_tracks(hdf5file, ids, track_features, obj_features, type)
    return(tracks)

####################################################################################
# private functions
####################################################################################
def _read_dimensions(hdf5file, frame_id, slice_id):
    dimension_path = f"/objects/frames/{frame_id}/slices/{slice_id}/dimensions"
    return(hdf5file[dimension_path][()])

def _read_object_by_path(hdf5file, object_path, features=["all"], image_dimension=None):
    import warnings
    supported_features = ["all", "none", "annotations", "bounding_box", "centroid",
                          "channel_id", "frame_id", "object_id", "outline", "slice_id"]
    unsupported = list(set(features) - set(supported_features))
    selected = list(set(features) & set(supported_features))

    if len(unsupported) != 0:
        warnings.warn(f"The following features '{unsupported}' are unsupported, continuing without...")

    if len(features) == 1:
        if features[0] == "all":
            selected = supported_features[2:len(supported_features)]
        else:
            selected = []
    else:
        selected = list(set(features) & set(supported_features[2:len(supported_features)]))
        if "all" in features:
            warnings.warn("The feature 'all' is ignored as other features are specified!")
        if "none" in features:
            warnings.warn("The feature 'none' is ignored as other features are specified!")

    object = [None if feature == "annotations" else hdf5file[f"{object_path}/{feature}"][()]
              for feature in selected]
    object = dict(zip(selected, object))

    mask_id = np.where(selected == "mask")[0]
    if len(mask_id) > 0:
        if image_dimension is None:
            ids = [hdf5file[f"{object_path}/{x}"][()] for x in ["frame_id", "slice_id"]]
            image_dimension = _read_dimensions(hdf5file, ids[0], ids[1])

        if len(object["bounding_box"]) == 0:
            bbox = hdf5file[f"{object_path}/{bounding_box}"][()]
        else:
            bbox=object["bounding_box"]

        object["mask_id"] = _create_mask(object["mask"], bbox, image_dimension)

    return(object)


def _read_objects(hdf5file, path, features=["all"], image_dimension=None):
    if f"{path}/objects" not in hdf5file:
        warnings.warn(f"Given path {path} is not a valid path containing objects!")
        return

    objects = _sort_paths([f"{path}/objects/{o}" for o in hdf5file[f"{path}/objects"]])
    ret = [_read_object_by_path(hdf5file, object, features, image_dimension) for object in objects]
    return (ret)

def _read_track(hdf5file, track_path, track_features=["all"], obj_features=["all"]):
    supported_features = ["all", "none", "annotations", "end", "id", "length", "next",
                          "next_event", "previous", "previous_event", "start"]
    unsupported = list(set(track_features) - set(supported_features))

    if len(unsupported) != 0:
        warnings.warn(f"The following features '{unsupported}' are unsupported, continuing without...")
    if len(track_features) == 1:
        if track_features[0] == "all":
            selected = supported_features[2:len(supported_features)]
        if track_features[0] == "none":
            selected = []
    else:
        selected = list(set(track_features) & set(supported_features[2:len(supported_features)]))
        if "all" in track_features:
            warnings.warn("The feature 'all' is ignored as other features are specified!")
        if "none" in track_features:
            warnings.warn("The feature 'none' is ignored as other features are specified!")

    if track_path.startswith("/autotracklets/"):
        selected = ["autotracklet_id" if x == "id" else x for x in selected]
    elif track_path.startswith("/tracklets/"):
        selected = ["tracklet_id" if x == "id" else x for x in selected]
    else:
        raise ValueError(f"Invalid track path '{track_path}'!")

    def _switch(feature):
        if feature == "annotations":
            return(None)
        elif feature == "length":
            return(hdf5file[f"{track_path}/end"][()] - hdf5file[f"{track_path}/start"][()])
        elif feature == "next":
            if f"{track_path}/next" in hdf5file:
                tracks = [f"{track_path}/next/{x}" for x in hdf5file[f"{track_path}/next"][()]]
                return(tracks)
            else:
                return(None)
        elif feature == "next_event":
            if f"{track_path}/next_event" in hdf5file:
                event = hdf5file[f"{track_path}/next_event/event_id"][()]
                return(event)
            else:
                return(None)
        elif feature == "previous":
            if f"{track_path}/previous" in hdf5file:
                tracks = [f"{track_path}/previous/{x}" for x in hdf5file[f"{track_path}/previous"][()]]
                return(tracks)
            else:
                return(None)
        elif feature == "previous_event":
            if f"{track_path}/previous_event" in hdf5file:
                event = hdf5file[f"{track_path}/previous_event/event_id"][()]
                return(event)
            else:
                return(None)
        else:
            print(f"{track_path}/{feature}")
            return(hdf5file[f"{track_path}/{feature}"][()])

    data = [_switch(feature) for feature in selected]

    objects = _read_objects(hdf5file, track_path, features=obj_features)
    objects_reordered = [
        [object[name] for object in objects]
        for name in list(objects[0].keys())]

    result = data + objects_reordered
    result = dict(zip(
        ["track_annotations" if x == "annotations" else x for x in selected] +
            list(objects[0].keys()),
        result))

    return(result)


def _read_tracks(hdf5file, ids=[-1], track_features=["all"], obj_features=["all"], type="tracklets"):
    if type == "autotracklets":
        if "/autotracklets" in hdf5file:
            autotracklet_paths = np.array([f"/autotracklets/{x}" for x in hdf5file["/autotracklets"]])
            autotracklet_ids = [hdf5file[f"{x}/autotracklet_id"][()] for x in autotracklet_paths]
            autotracklet_paths = autotracklet_paths[np.argsort(autotracklet_ids)]
            autotracklet_ids = np.sort(autotracklet_ids)
        else:
            warnings.warn("File contains no autotracklets!")
            return

    if type == "tracklets":
        if "/tracklets" in hdf5file:
            tracklet_paths = np.array([f"/tracklets/{x}" for x in hdf5file["/tracklets"]])
            tracklet_ids = [hdf5file[f"{x}/tracklet_id"][()] for x in tracklet_paths]
            tracklet_paths = tracklet_paths[np.argsort(tracklet_ids)]
            tracklet_ids = np.sort(tracklet_ids)
        else:
            warnings.warn("File contains no tracklets!")
            return

    if type == "autotracklets":
        available_ids = autotracklet_ids
        paths = autotracklet_paths
    elif type == "tracklets":
        available_ids = tracklet_ids
        paths = tracklet_paths
    else:
        warnings.warn("Value of type has to be either 'autotracklets' or 'tracklets!")
        return

    if len(ids) == 1  and ids[0] == -1:
        ret = [_read_track(hdf5file, path, track_features, obj_features) for path in paths]
        return(ret)
    else:
        notfound = list(set(ids) - set(available_ids))
        if len(notfound) > 0:
            warnings.warn(f"Tracks with the id's {notfound} do not exist, continuing without...")

        ret = [_read_track(hdf5file, path, track_features, obj_features) for path in paths[available_ids & ids]]
        return(ret)


def _sort_paths(paths):
    idx = np.argsort([int(path.split("/")[-1]) for path in paths])
    paths = np.array(paths)[idx]
    return(paths)
