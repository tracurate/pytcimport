from setuptools import setup

setup(name='tcimport',
      version='0.1',
      description='A package for accessing files in Tracurate\'s HDF5 file format',
      url='https://gitlab.com/tracurate/pytcimport',
      author='Sebastian Wagner',
      author_email='sebastian (dot) wagner3 (at) tu-dresden (dot) de',
      license='GPL-3',
      packages=['tcimport'],
      install_requires=['h5py', 'matplotlib', 'numpy'],
      python_requires='~=3.6',
      zip_safe=False)
